# Vim Notes

From the termina, I typed the command `vim vim_notes.md`.

In here I then pressed i to get into insert mode.  And wrote this note.
I then press escape to get out of insert mode and started the
proofreading and edit process.

## Movement in VIM

The movement keys in vim simply are hjkl, where each key, from left to
right, will move left, down, up, right one character.  The j and k will
move one character by line.  Or simply, will move down or up one line.

Quicker movement keys are w for word, b for beginning of word, e for
ending of word.  Go ahead and practice this now, let's get to the error
that I made where I forgot a space in 'beginning of'.

when not in insert mode, press o to open a new line.
* cw to change word.
* dw to delete word.
* dd to delete line.
* { for paragraph moving.
* u for undo.
* r to replace character.
* O opens line above.
* xp does a delete of the character that we are on, and the p places it
* after our current character position

In fact, the d or c are verbs, and the 2nd letter is the object of ouru
desire.  What we want that verb to act on.  So we are not limited to w
as a word, we could also change beginning (or change from where we are
to the beginning of a word).

We can also use a number to move to more than a single place.

For almost all things you can expect that we can give a number to
increase the command.

For further training on Vim, you can run the vimtutor command on your
terminal
