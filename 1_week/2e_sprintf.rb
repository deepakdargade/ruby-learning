# Exercise 2
# Read the sprintf documentation and the % documentation in the String class and figure out the output being printed by of this Ruby code.

puts '%05d' % 123

# OK, so what is going on with this exercise?  What is the output, what is the return and why does it do what it does?
# Write in here please :)

# Where do we find information about this?  How can we use it for our own (perhaps nefarious) purposes?
# where we search the info about the sprintf and %  We can use ri on our own system to get to the documentation.  If it is installed properly.
# Otherwise we can go to http://ruby-doc.org

# String#% is how we annotate this in writing.  The String being the class and the # signifying an instance method.
#
# If it were a class method we would state String.%.  See the difference?
#
# When we see the String#% method documentation we get this information:  "See Kernel sprintf method"
#
# So our real documentation is in Kernel#srpintf
#

# Ruby Interactive is 'ri' as a command on the terminal.  If you do ri Array an
# do not get the Core Array information, then the ri documentation for the Ruby
# core is not installed.

puts 'What is your name? '
name = gets.chomp
puts 'What is your age? '
age = gets.to_i
puts 'What is your favorite color? '
color = gets.chomp
report_string = 'Your name is %10<name>s.  I know you are %<age>i years old. You like the color %<color>s.'
my_hash = { name: name, age: age, color: color }
puts report_string % my_hash

puts(format(report_string, my_hash))
