# Exercise4. Write a Ruby program that tells you how many minutes there are in
# a year (do not bother right now about leap years etc.).
#
# minutes_in_a_year method will return the number of minutes in a number
# of years
#
#     doctest: Number of minutes in a year
#     >>  minutes_in_a_years
#     => 525600
#
#     doctest: Number of minutes 3 years
#     >>  minutes_in_a_years 3
#     => 1576800
#
def minutes_in_a_years year = 1
  year * 365 * 24 * 60
end
