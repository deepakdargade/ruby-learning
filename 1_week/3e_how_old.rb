# Exercise3. Write a Ruby program that displays how old I am, in years, if I am
# 979000000 seconds old. Display the result as a floating point (decimal)
# number to two decimal places (for example, 17.23).
#
# Note: To format the output to say 2 decimal places, we can use the Kernel's
# format method.  For example, if x = 45.5678 then format("%.2f", x) will
# return the string 45.57
#
# Notes on organizing our code:  Methods goes above running code


# doctest: Setup for our tests
# >> age_in_seconds = 9.79e8

# doctest: my age is 31.04
# >> age_in_years(age_in_seconds).round(2)
# => 31.04
#
# doctest: Bug found, where integer argument produced wrong answer
# >> age_in_years(1_000_000).round(2)
# => 0.03
def age_in_years age_in_seconds
  age_in_seconds / 365.0 / 24 / 60 / 60
end

# This is called a application guard or a library guard.
# This block of "running code" will only run if loaded as a library
# or for a test suite.
if __FILE__ == $0
  # doctest: my age is 979000000
  # >> age_in_seconds
  # => 979000000
  #
  age_in_seconds = 9.79e8

  # We are using an Array for our argument to our String#% method
  # because we have two values we want to format.
  puts "If your age is %i then you are %.2f years old." % [age_in_seconds, age_in_years(age_in_seconds)]
end
